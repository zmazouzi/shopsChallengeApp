import React, {Component} from 'react';
import './App.css';
import {withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';

import MenuIcon from '@material-ui/icons/Shop';
import FavoriteIcon from '@material-ui/icons/Favorite';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import Shops from "./components/shops/Shops";
import SignIn from "./components/auth/SignIn";
import {auth, firestore} from "./config/firebase";
import LikedShops from "./components/likedShops/likedShops";
import NearByShops from "./components/neareByShops/NearByShops";


const styles = {
	root: {
		flexGrow: 1,
	},
	flex: {
		flexGrow: 1,
	},
	menuButton: {
		marginLeft: -12,
		marginRight: 20,
	},
};

class App extends Component {

	state = {open: true, userId: "", choice: 0};


	getCurrentLocation = (userId) => {
			navigator.geolocation.getCurrentPosition((res) => {
				const {latitude,longitude} = res.coords;
				this.setState({currentPosition : {latitude,longitude}});
				console.log(latitude,longitude);
				firestore.doc(`users/${userId}`).set({ currentPosition: {latitude, longitude}},{merge: true})
			})
	};

	getNearByShops() {
		this.getCurrentLocation(this.state.userId);
	}

	componentDidMount() {

		auth.onAuthStateChanged(user => {
			if (user) {
				this.setState({open: false, userId: user.uid});
				const {email} = user.providerData[0];
				firestore.doc(`users/${user.uid}`).set({email}, {merge: true});
				this.getNearByShops();
			}
			else {
				this.setState({open: true})
			}
		})
	}

	render() {
		const {classes} = this.props;
		return (
			<div className={classes.root}>
				<AppBar position="static" style={{height: "10vh"}}>
					<Toolbar>
						<IconButton onClick={() => {
							this.setState({choice: 0})
						}} className={classes.menuButton} color="inherit" aria-label="Menu">
							<MenuIcon/>
						</IconButton>
						<Typography variant="title" color="inherit" className={classes.flex}>
							Shops App challenge application
						</Typography>
						<Button color="inherit" onClick={() => {
							this.setState({choice: 2})
						}}>
							<LocationOnIcon className="icon-header"/>
							Nearby Shops</Button>
						<Button onClick={() => {
							this.setState({choice: 1})
						}} color="inherit">
							<FavoriteIcon className="icon-header"/>
							My preferred Shops</Button>
					</Toolbar>
				</AppBar>
				<SignIn open={this.state.open}/>
				{this.state.choice === 0 && <Shops userId={this.state.userId}/>}
				{this.state.choice === 1 && <LikedShops userId={this.state.userId}/>}
				{this.state.choice === 2 && <NearByShops currentPosition={this.state.currentPosition} userId={this.state.userId}/>}
			</div>
		)

	}
}

export default withStyles(styles)(App);
