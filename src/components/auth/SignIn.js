import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import blue from '@material-ui/core/colors/purple';
import {auth} from '../../config/firebase';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import {withStyles} from "@material-ui/core/styles/index";
import SignUp from './SignUp';
import Snackbar from '@material-ui/core/Snackbar';

const styles = theme => ({
	textField: {
		marginLeft: theme.spacing.unit,
		marginRight: theme.spacing.unit,
		width: 200,
	},
	snackbar: {
		margin: theme.spacing.unit,
	},
});


class SignIn extends React.Component {
	state = {
		open: this.props.open,
		email: "",
		password: "",
		signUPOpen: false,
		snack: false,
		errMessage: ""
	};
	handleChange = name => event => {
		this.setState({
			[name]: event.target.value,
		});
	};
	handleClickOpen = () => {
		this.setState({open: true});
	};
	signIn = (email, password) => {
		auth.signInWithEmailAndPassword(email, password)
			.then(res => {
				console.log(res)
			})
			.catch(err => {
				console.log(err.message);
				this.setState({errMessage: err.message, snack: true})
				setTimeout(() => this.setState({snack: false}), 5000)
			})
		;
	};
	handleClose = () => {
		this.setState({open: false});
	};


	render() {
		const {classes} = this.props;
		return (<div>
				<Dialog
					style={{margin: 0, padding: 0}}
					open={this.props.open}
					onClose={() => {
						console.log("not closing until sign up")
					}}
					aria-labelledby="alert-dialog-title"
					aria-describedby="alert-dialog-description"
				>
					<DialogTitle id="alert-dialog-title" style={{backgroundColor: blue[500], color: "inherit"}}>

						Sign up to see shops

					</DialogTitle>
					<DialogContent>

						<Grid container direction={"column"} style={{width: 400}}>
							<Grid item>
								<TextField
									id="email"
									label="Email"
									fullWidth
									value={this.state.email}
									onChange={this.handleChange('email')}
									margin="normal"
								/>
							</Grid>
							<Grid item>
								<TextField
									id="password"
									label="Password"
									fullWidth
									value={this.state.password}
									type={"password"}
									onChange={this.handleChange('password')}
									margin="normal"
								/>
							</Grid>
						</Grid>
					</DialogContent>
					<DialogActions>

						<Button onClick={() => {
							this.setState({signUPOpen: true})
						}} color="primary">
							SIGN UP
						</Button>
						<Button disabled={this.state.email === "" || this.state.password === ""} onClick={() => {
							this.signIn(this.state.email, this.state.password)
						}} color="primary">
							SIGN IN
						</Button>


						<SignUp open={this.state.signUPOpen}/>

					</DialogActions>
				</Dialog>
				<Snackbar
					open={this.state.snack}
					onClose={() => {
						console.log("hey")
					}}
					className={classes.snackbar}
					anchorOrigin={{vertical: "bottom", horizontal: "center"}}
					message={`${this.state.errMessage}`}/>
			</div>
		);
	}


}

export default withStyles(styles)(SignIn);
