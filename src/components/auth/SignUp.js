import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import blue from '@material-ui/core/colors/purple';
import {auth} from '../../config/firebase';
import {withStyles} from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';

const styles = theme => ({
	textField: {
		marginLeft: theme.spacing.unit,
		marginRight: theme.spacing.unit,
		width: 200,
	}
});


class SignUp extends React.Component {
	state = {
		email: "",
		password: "",
		rpassword: ""
	};

	handleChange = name => event => {
		this.setState({
			[name]: event.target.value,
		});
	};

	handleClickOpen = () => {
		this.setState({open: true});
	};

	signUp = (email, password) => {
		auth.createUserWithEmailAndPassword(email, password)
			.then(res => {
				console.log(res);
			})
			.catch(err => {
				console.log(err);
			})
	};
	handleClose = () => {
		this.setState({open: false});
	};


	render() {
		return (<div>
				<Dialog
					style={{margin: 0, padding: 0}}
					open={this.props.open}
					onClose={() => {
						console.log("not closing until sign up")
					}}
					aria-labelledby="alert-dialog-title"
					aria-describedby="alert-dialog-description"
				>
					<DialogTitle id="alert-dialog-title" style={{backgroundColor: blue[500], color: "inherit"}}>

						Sign up to see shops

					</DialogTitle>
					<DialogContent>

						<Grid container direction={"column"} style={{width: 400}}>
							<Grid item>
								<TextField
									id="email"
									label="Email"
									fullWidth
									value={this.state.email}
									onChange={this.handleChange('email')}
									margin="normal"
								/>
							</Grid>
							<Grid item>
								<TextField
									id="password"
									label="Password"
									fullWidth
									value={this.state.password}
									type={"password"}
									onChange={this.handleChange('password')}
									margin="normal"
								/>
							</Grid>
							<Grid item>
								<TextField
									id="rpasswor"
									label="Repeat password"
									fullWidth
									value={this.state.rpassword}
									type={"password"}
									onChange={this.handleChange('rpassword')}
									margin="normal"
								/>
							</Grid>
						</Grid>
					</DialogContent>
					<DialogActions>

						<Button disabled={this.state.email === "" || this.state.password === ""} onClick={() => {
							this.signUp(this.state.email, this.state.password)
						}} color="primary">
							SIGN UP
						</Button>

					</DialogActions>
				</Dialog>
			</div>
		);
	}
}

export default withStyles(styles)(SignUp);
