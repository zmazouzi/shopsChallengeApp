import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {firestore} from "../../config/firebase";

const styles = {
	card: {
		maxWidth: 250,
	},
	button: {
		color: "green"
	},
	media: {
		height: 0,
		paddingTop: '56.25%', // 16:9
	},
};


class ShopCard extends React.Component {


	state = {liked: false};

	componentWillMount() {
		// this.setState({liked: this.getShoplike(this.props.uid)})
		this.getShoplike(this.props.uid)
	}

	likeShop(uid, like) {
		console.log(uid, this.props.userId);
		firestore.doc(`likes/${uid}`).set({[this.props.userId]: like});
		this.getShoplike(uid);
		if (like) {
			firestore.doc(`users/${this.props.userId}/likedShops/${uid}`)
				.set(this.props.shop);
		}
	}


	getShoplike(uid) {
		firestore.doc(`likes/${uid}`).get()
			.then(res => {
				const liked = res.data();
				if (typeof(liked) !== "undefined") {
					console.log(liked[this.props.userId])
					this.setState({liked: liked[this.props.userId]})
				}
			})
			.catch(err => {
				console.log(err)
			})
	}

	render() {
		const {classes} = this.props;
		const shop = this.props.shop;
		const liked = this.state.liked;
		return (
			<div>
				<Card className={classes.card}>
					<CardMedia
						className={classes.media}
						image={shop.picture}
						title={shop.city}
					/>
					<CardContent>
						<Typography gutterBottom variant="headline" component="h2">
							{shop.name}
						</Typography>
						<Typography component="p">
							{shop.city}
						</Typography>
						<Typography component="p">
							{shop.email}
						</Typography>
					</CardContent>
					<CardActions>
						<Button disabled={!liked} onClick={() => {
							this.likeShop(this.props.uid, false)
						}} size="small" variant={"raised"} color={"secondary"}>
							DISLIKE
						</Button>
						<Button disabled={liked} onClick={() => {
							this.likeShop(this.props.uid, true)
						}} size="small" variant={"raised"} className={classes.button}>
							LIKE
						</Button>
					</CardActions>
				</Card>
			</div>
		);
	}
}


export default withStyles(styles)(ShopCard);