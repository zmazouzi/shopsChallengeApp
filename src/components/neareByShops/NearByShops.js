import React, {Component} from 'react';
import ShopCard from './NearByShopCard'
import {auth, firestore, snapShotToList} from "../../config/firebase";
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import AddIcon from '@material-ui/icons/ArrowBack';
import {withStyles} from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import * as _ from 'lodash';

const styles = theme => ({
	fab: {
		position: 'fixed',
		bottom: theme.spacing.unit * 2,
		right: theme.spacing.unit * 2,
	}
});

class Shops extends Component {

	state = {shops: [], fetching: true};
	signOut = () => {
		auth.signOut()
			.then(res => {
				console.log(res)
			})
			.catch(err => {
				console.log(err)
			})
	};

	componentWillMount() {
		this.getShops();
	}

	getShops() {
		this.setState({fetching: true});
		firestore.collection("stores").get()
			.then(res => {
				console.log(snapShotToList(res));
				const ref = this.props.currentPosition;
				const nearByShops = _.find(snapShotToList(res), function (o) {
					if (typeof(o) !== "undefined" && typeof(o.citizen) !== "undefined") {
						alert(o.citizen.location);
						return o.citizen.location.coordinates[0] - ref.latitude < 0.0001 && o.citizen.location.coordinates[1] - ref.longitude < 0.0001;
					}
				});
				this.setState({shops: (nearByShops) ? nearByShops : [], fetching: false});
			})
			.catch(err => {
				console.log(err)
			})
	}

	render() {
		const {classes} = this.props;
		const fetching = this.state.fetching;
		return (
			<div>
				{fetching && <LinearProgress/>}

				<div style={{height: "90vh", padding: 20}}>

					<Grid container spacing={16} justify={"center"} alignItems={"center"}>
						{
							this.state.shops.map((shop, index) => {
								return <Grid item key={index}>
									<ShopCard getShops={() => {
										this.getShops()
									}} userId={this.props.userId} uid={shop.uid} shop={shop.data.citizen}/>
								</Grid>
							})
						}
					</Grid>

					<Button onClick={() => {
						this.signOut()
					}} variant="fab" color="primary" className={classes.fab}>
						<AddIcon/>
					</Button>

				</div>
			</div>
		);
	}
}


export default withStyles(styles)(Shops);
