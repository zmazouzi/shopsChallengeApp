import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import 'firebase/firestore';

const config =  {
	apiKey: "AIzaSyCceKzGmlQPBH63SjHSJfmocp5APSrk118",
	authDomain: "nextmediachallenge.firebaseapp.com",
	databaseURL: "https://nextmediachallenge.firebaseio.com",
	projectId: "nextmediachallenge",
	storageBucket: "",
	messagingSenderId: "331368795308"
};

export const fire = firebase.initializeApp(config);
//fireStore Config
let firestoreInit = firebase.firestore();
const settings = {timestampsInSnapshots: true};
firestoreInit.settings(settings);
export const firestore = firestoreInit;


// auth config
export const auth = fire.auth();

// querySnapshot to list
export function snapShotToList(querySnapshot) {
	let list = [];
	querySnapshot.forEach(doc => {
		list.push({uid: doc.id, data: doc.data()});
	});
	return list;
}