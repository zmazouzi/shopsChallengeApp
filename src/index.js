import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import purple from '@material-ui/core/colors/purple';
import red from '@material-ui/core/colors/red';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';


const theme = createMuiTheme({
	palette: {
		primary: { main: purple[500] },
		secondary: { main: red[500] },
	},
	shape: {
		borderRadius: 25
	}
});

ReactDOM.render(
	<MuiThemeProvider theme={theme}>
			<App  />
	</MuiThemeProvider>

	, document.getElementById('root'));
registerServiceWorker();
